//
//  MessagesTableViewCell.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 02/01/2022.
//

import UIKit
import Kingfisher

class MessagesTableViewCell: UITableViewCell {

    @IBOutlet weak var senderImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var unseenCountView: UIView!
    @IBOutlet weak var unseenCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(_ viewModel: MainMessages.FetchMessages.ViewModel.DisplayMessage){
        switch viewModel.senderImage {
        case .group:
            senderImageView.image = R.image.icon_group()
        case .individual(url: let url):
            senderImageView.kf.setImage(with: URL(string: url), placeholder: R.image.icon_profile())
        }
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        timeLabel.text = viewModel.time
        unseenCountView.isHidden = viewModel.unseenCount < 1
        unseenCountLabel.text = "\(viewModel.unseenCount)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
