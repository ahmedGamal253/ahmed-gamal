//
//  MainMessagesInteractor.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 20/12/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainMessagesBusinessLogic {
    func fetchProfileAndMessages()
}

protocol MainMessagesDataStore {
    var messages: [Message]? { get set }
}

class MainMessagesInteractor: MainMessagesBusinessLogic, MainMessagesDataStore {
    
    var messages: [Message]?
    var presenter: MainMessagesPresentationLogic?
    var worker: MainMessagesWorker?
    //var name: String = ""

    // MARK: - Do something

    func fetchProfileAndMessages(){
        self.getProfile { (sucess) in
            if sucess{
                self.getMessages()
            }
        }
    }
    
   private func getProfile(completion: @escaping (Bool) -> Void) {
        presenter?.presentShowIndicator()
        worker = MainMessagesWorker()
        worker?.getProfile(completion: { [weak self](result) in
            guard let self = self else { return}
            self.presenter?.presentHiddenIndicator()
            switch result{
            case .success(let profile):
                self.worker?.storeMyProfile(request: MainMessages.Profile.StoreProfile.Request(profile: profile))
                self.presenter?.presentMyProfile(response: MainMessages.Profile.FetchProfile.Response(Profile: profile))
                completion(true)
            case .failure( let error):
                completion(false)
                if let error = error as? AppError{
                    self.presenter?.showError(error: error)
                }else{
                    self.presenter?.showError(error: AppError.serverError)
                }
            }
        })
    }
    
    private func getMessages(){
        presenter?.presentShowIndicator()
        worker = MainMessagesWorker()
        worker?.getMessage(completion: { [weak self](result) in
            guard let self = self else { return}
            self.presenter?.presentHiddenIndicator()
            switch result{
            case .success(let messages):
                if messages.data.isEmpty{
                    self.presenter?.showError(error: .noMessage)
                }else{
                    self.messages = messages.data
                    self.presenter?.presentMessages(response: MainMessages.FetchMessages.Response(Messages: messages.data))
                }
            case .failure( let error):
                if let error = error as? AppError{
                    self.presenter?.showError(error: error)
                }else{
                    self.presenter?.showError(error: AppError.serverError)
                }
            }
        })
    }
}
