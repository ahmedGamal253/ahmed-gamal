//
//  LoginModels.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 20/12/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Login {
    // MARK: - Use cases

    enum makeLogin {
        struct Request {
            let userName: String
            let password: String
        }
        struct Response { }
        struct ViewModel { }
    }
    
    enum storeData{
        struct Request {
            let token: Token
        }
    }
}
