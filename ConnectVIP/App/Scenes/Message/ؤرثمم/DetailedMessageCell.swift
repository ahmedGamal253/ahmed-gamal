//
//  DetailedMessageCell.swift
//  Connect
//
//  Created by ExpertApps on 14/02/2021.
//

import UIKit

protocol MessageCellDelegate: class {
    // exbandable label
    func wilExpand()
    func didExpanded()
    func willCollapse()
    func didCollapsed()
    // bottom buttons
    func attachmentsTapped(id: Int)
    func commentsTapped(id: Int)
    func recipientsTapped(id: Int)
}

extension MessageCellDelegate {
    // setting default implementation
    func attachmentsTapped(id: Int) {}
    func commentsTapped(id: Int) {}
    func recipientsTapped(id: Int) {}
}

class DetailedMessageCell: UITableViewCell {
    
    @IBOutlet weak var superBackView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var senderButton: UIButton!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var readMoreLessButton: UIButton!
    
    @IBOutlet weak var attachmentsButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var recipientsButton: UIButton!
    
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    weak var delegate: MessageCellDelegate?
    private var isCollapsed: Bool = true
    private let readMoreText = R.string.localizable.seeMore()
    private let readLessText = R.string.localizable.seeLess()
    
    var viewModel: MessageHistory.FetchMessageThread.ViewModel.DisplayMessage! {
        didSet {
            senderNameLabel.text = viewModel.senderName
            senderButton.kf.setImage(with: URL(string: viewModel.senderImageURL), for: .normal,placeholder: UIImage(named: "icon_profile"))
            timeLabel.text = viewModel.date
            subjectLabel.text = viewModel.subject
           // readMoreLessButton.isHidden = messageLabel.calculateMaxLines() <= 3
           // readMoreLessButton.isHidden = countLabelLines(label: messageLabel) < 3
            
            recipientsButton.isHidden = viewModel.recipientsCount < 1
            // set localize ui 
            recipientsButton.setTitle("\(R.string.localizable.recipients())(\(viewModel.recipientsCount))", for: .normal)
            commentsButton.setTitle("\(R.string.localizable.comments())", for: .normal)
            attachmentsButton.setTitle(R.string.localizable.attachments(), for: .normal)
            let bottomButtons = [attachmentsButton, commentsButton, recipientsButton]
            switch viewModel.type {
            case .other:
                backView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
                backView.backgroundColor = .white
                leadingConstraint.constant = 16
                trailingConstraint.constant = 36
                bottomButtons.forEach { button in
//                    button?.tintColor = DesignSystem.Color.title.uicolor
//                    button?.setTitleColor(DesignSystem.Color.title.uicolor, for: .normal)
                }
            case .user:
                backView.layer.maskedCorners = [.layerMinXMinYCorner ,.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
               // backView.backgroundColor = DesignSystem.Color.userMessageBackground.uicolor
                leadingConstraint.constant = 36
                trailingConstraint.constant = 16
                bottomButtons.forEach { button in
//                    button?.tintColor = DesignSystem.Color.darkTitle.uicolor
//                    button?.setTitleColor(DesignSystem.Color.darkTitle.uicolor, for: .normal)
                }
            }
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // setup back view corner radius
        backView.layer.cornerRadius = 20.0
        backView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        readMoreLessButton.setTitle(readMoreText, for: .normal)
        senderButton.imageView?.contentMode = .scaleAspectFill
    }
    
    @IBAction func readMoreLessButtonTapped(_ sender: Any) {
        
        if isCollapsed {
            delegate?.wilExpand()
            messageLabel.numberOfLines = 0
            isCollapsed.toggle()
            readMoreLessButton.setTitle(readLessText, for: .normal)
            delegate?.didExpanded()
        } else {
            delegate?.willCollapse()
            messageLabel.numberOfLines = 2
            isCollapsed.toggle()
            readMoreLessButton.setTitle(readMoreText, for: .normal)
            delegate?.didCollapsed()
        }
        
    }
    
    @IBAction func attachmentsButtonTapped(_ sender: Any) {
       // delegate?.attachmentsTapped(id: viewModel.id)
    }
    
    @IBAction func commentsButtonTapped(_ sender: Any) {
        //delegate?.commentsTapped(id: viewModel.id)
    }
    
    @IBAction func recipientsButtonTapped(_ sender: Any) {
        //delegate?.recipientsTapped(id: viewModel.id)
    }
    
}
