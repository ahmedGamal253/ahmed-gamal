//
//  UIViewControllerExtensions.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 22/12/2021.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    func showIndicator(_ show: Bool = true) {
        if show {
            let size = CGSize(width: 70, height: 70)
            startAnimating (size , type: .ballSpinFadeLoader, color: R.color.secondary(), padding: 10)
        } else {
            stopAnimating()
        }
    }
    
    func showAlert(title: String,message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
                case .default:
                print("default")
                case .cancel:
                print("cancel")
                
                case .destructive:
                print("destructive")
                
            @unknown default:
                print("error in UIAlertController")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
