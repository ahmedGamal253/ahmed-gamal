//
//  String+Extensions.swift
//  Connect
//
//  Created by ExpertApps on 28/02/2021.
//

import Foundation
import CommonCrypto

extension String {
    
    var MD5: String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        
        if let d = self.data(using: .utf8) {
            _ = d.withUnsafeBytes { body -> String in
                CC_MD5(body.baseAddress, CC_LONG(d.count), &digest)
                
                return ""
            }
        }
        
        return (0 ..< length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
    
    var backslashRemoved: String {
        return self.replacingOccurrences(of: "\\", with: "/")
    }
        
    var attributesStringFromHTMLURLEncoded: NSAttributedString {
        let htmlData = NSString(string: self).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        return attributedString
    }
    
    var toAppDate: String {
        
        let apiDateFormatter = DateFormatter()
        apiDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let appDateFormatter = DateFormatter()
        appDateFormatter.dateFormat = "MMMM d, yyyy"
        
        let apiDate = apiDateFormatter.date(from: self) ?? Date()
        return appDateFormatter.string(from: apiDate)
    }
    
    var toAppTime: String {
        
        let apiDateFormatter = DateFormatter()
        apiDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let appTimeFormatter = DateFormatter()
        appTimeFormatter.dateFormat = "h:mm a"
        
        let apiDate = apiDateFormatter.date(from: self) ?? Date()
        return appTimeFormatter.string(from: apiDate)
        
    }
    
    var toAppDateTime: String {
        
        let apiDateFormatter = DateFormatter()
        apiDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let appDateFormatter = DateFormatter()
        appDateFormatter.dateFormat = "MMMM d, yyyy"
        appDateFormatter.dateStyle = .medium
        appDateFormatter.doesRelativeDateFormatting = true
        
        let appTimeFormatter = DateFormatter()
        appTimeFormatter.dateFormat = "h:mm a"
        
        let apiDate = apiDateFormatter.date(from: self) ?? Date()
        return "\(appDateFormatter.string(from: apiDate)), \(appTimeFormatter.string(from: apiDate))"
        
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isValidPassword: Bool {
        let password_regex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", password_regex)
        return emailTest.evaluate(with: self)
    }
    
    func ifEmptyDefaultTo(_ defaultValue: String) -> String {
        isEmpty ? defaultValue : self
    }
    
    
    
}

extension Optional where Wrapped == String {
    
    var orEmpty: String {
        self ?? ""
    }
    
    var isNilOrEmpty: Bool {
        self?.isEmpty ?? true
    }
    
}
