//
//  MessageService.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 02/01/2022.
//

import Foundation
import Moya

enum MessageService {
    case Messages
    case MessageDetails(id:ConversationIDType)
}

extension MessageService: TargetType, AccessTokenAuthorizable {

    var baseURL: URL {
        return URL(string: "https://connectcoreapi.expertapps.com.sa/api")!
    }
    
    var path: String {
        switch self {
        case .Messages, .MessageDetails:
            return "messages/messages"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .Messages, .MessageDetails:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .Messages:
            return .requestJSONEncodable(
                MessagesBody(pageSize: 15 , pageNumber: 0))
        case .MessageDetails(let id):
            return .requestJSONEncodable(
                MessagesBody(pageSize: 15, pageNumber: 0, searchMessageModel: SearchMessageModel(idType: id))
            )
        }
    }
    
    var headers: [String : String]? {
        [
            "lang" : "en",
            "clientId" : "2"
        ]
    }
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .Messages,.MessageDetails:
            return .bearer
        }
    }
}

fileprivate struct MessagesBody: Encodable {
    let pageSize: Int
    let pageNumber: Int
    let searchMessageModel: SearchMessageModel
    let currentMessageTab: Int = 0
    
    init(pageSize: Int ,pageNumber: Int) {
        self.pageSize = pageSize
        self.pageNumber = pageNumber
        self.searchMessageModel = SearchMessageModel(idType: .none)
    }
    
    init(pageSize: Int ,pageNumber: Int, searchMessageModel: SearchMessageModel) {
        self.pageSize = pageSize
        self.pageNumber = pageNumber
        self.searchMessageModel = searchMessageModel
    }
}

fileprivate struct SearchMessageModel: Codable {
    let groupIdFilter: String?
    let individualIdFilter: String?
    let searchText: String?
    
    init(idType: ConversationIDType, text: String = "") {
        switch idType {
        case .none:
            self.groupIdFilter = nil
            self.individualIdFilter = nil
        case .group(let groupID):
            self.groupIdFilter = groupID
            self.individualIdFilter = nil
        case .individual(let individualID):
            self.groupIdFilter = nil
            self.individualIdFilter = individualID
        }
        self.searchText = text
    }
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case groupIdFilter = "groupIdFilter"
        case individualIdFilter = "indvidualIdFilter"
        case searchText = "searchText"
    }
}

enum ConversationIDType {
    case none
    case group(String)
    case individual(String)
}
