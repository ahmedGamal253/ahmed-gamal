//
//  AuthService.swift
//  Connect
//
//  Created by Kerolles Roshdi on 2/10/21.
//

import Foundation
import Moya

enum AuthService {
    case login(username: String, password: String)
    case myProfile
}

extension AuthService: TargetType, AccessTokenAuthorizable {

    var baseURL: URL {
        return URL(string: "https://connectcoreapi.expertapps.com.sa/api")!
    }
    
    var path: String {
        switch self {
        case .login:
           // return "token"
            return "Authenticate/login"
        case.myProfile:
            return "accounts/userProfile"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .myProfile:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case let .login(username, password):
            return .requestParameters(
                parameters: [
                    "username" : username,
                    "password" : password,
                    "grant_type" : "password",
                    "device_type" : 1,
                    "deviceId": ""
                ],
                encoding: JSONEncoding.default
            )
        case .myProfile:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        [
            "lang" : "en",
            "clientId" : "2"
        ]
    }
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .login:
            return .none
        case .myProfile:
            return .bearer
        }
    }
}
