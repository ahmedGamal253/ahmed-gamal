//
//  MessageDataSource.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 02/01/2022.
//

import Foundation
import Moya

class MessageDataSource {
    
    private var messageServiceProvider = MoyaProvider<MessageService>()
    private var localUserDataSource = LocalUserDataSource()
    init() {
        let accessToken = localUserDataSource.getToken()?.accessToken ?? "NO ACCESS TOKEN"
       // let refreshToken = localUserDataSource.getProfile()?.refreshToken ?? "NO REFRESH TOKEN"
        let loggerConfig = NetworkLoggerPlugin.Configuration(logOptions: .verbose)
        let networkLogger = NetworkLoggerPlugin(configuration: loggerConfig)
        let tokenPlugin = VariableAccessTokenPlugin(tokenClosure: { accessToken })
        self.messageServiceProvider = .init(plugins: [networkLogger,tokenPlugin])
    }
    
    func getmessages(completion: @escaping (Result<MessagesResponseModel, Error>) -> Void) {
        messageServiceProvider.request(.Messages) { result in
            completion(MoyaHelpers.consumeMoyaResult(result))
        }
    }
    
    func getChatDetails(id:ConversationIDType,completion: @escaping (Result<Page<DetailedMessage>,Error>)-> Void){
        messageServiceProvider.request(.MessageDetails(id: id)) { result in
            completion(MoyaHelpers.consumeMoyaResult(result))
        }
    }
}
