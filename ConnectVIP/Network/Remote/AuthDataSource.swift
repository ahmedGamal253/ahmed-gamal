//
//  AuthDataSource.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 22/12/2021.
//

import Foundation
import Moya

class AuthDataSource {
    
    private var authServiceProvider = MoyaProvider<AuthService>()
    private var localUserDataSource = LocalUserDataSource()
    
    init() {
        let accessToken = localUserDataSource.getToken()?.accessToken ?? "NO ACCESS TOKEN"
       // let refreshToken = localUserDataSource.getProfile()?.refreshToken ?? "NO REFRESH TOKEN"
        let loggerConfig = NetworkLoggerPlugin.Configuration(logOptions: .verbose)
        let networkLogger = NetworkLoggerPlugin(configuration: loggerConfig)
        let tokenPlugin = VariableAccessTokenPlugin(tokenClosure: { accessToken })
        self.authServiceProvider = .init(plugins: [networkLogger,tokenPlugin])
    }
    
    func login(username: String, password: String, completion: @escaping (Result<Token, Error>) -> Void) {
        authServiceProvider.request(.login(username: username, password: password)) { result in
            completion(MoyaHelpers.consumeMoyaResult(result))
        }
    }
    
    func getProfile(completion: @escaping (Result<UserProfile, Error>) -> Void){
        authServiceProvider.request(.myProfile) { result in
            completion(MoyaHelpers.consumeMoyaResult(result))
        }
    }
}
