//
//  LocalUserDataSource.swift
//  Connect
//
//  Created by Kerolles Roshdi on 2/10/21.
//

import Foundation
import KeychainAccess

class LocalUserDataSource {
    
    private let keychain: Keychain
    private let tokenKey = "connect.user.token"
    private let profileKey = "connect.user.profile"
    private let isLoggedInKey = "connect.user.isLoggedIn"
    private let deviceTokenKey = "connect.device.token"
    private let pushNotificationValueKey = "push.notification.value"
    
    init(provider: Keychain = Keychain(service: "connect.user")) {
        self.keychain = provider
    }
    
    func storeToken(_ token: Token) {
        do {
            let tokenData = try JSONEncoder().encode(token)
            try keychain.set(tokenData, key: tokenKey)
        } catch let error {
            debugPrint(" ---- error storing user token: \(error)")
        }
    }
    
    func getToken() -> Token? {
        guard let tokenData = try? keychain.getData(tokenKey) else { return nil }
        return try? JSONDecoder().decode(Token.self, from: tokenData)
    }
    
    func removeToken() {
        do {
            try keychain.remove(tokenKey)
        } catch let error {
            debugPrint(" ---- error removing user token: \(error)")
        }
        
    }
    
    func storeProfile(_ profile: UserProfile) {
        do {
            let profileData = try JSONEncoder().encode(profile)
            try keychain.set(profileData, key: profileKey)
        } catch {
            debugPrint(" ---- error storing profile: \(error)")
        }
    }
    
    func getProfile() -> UserProfile? {
        guard let profileData = try? keychain.getData(profileKey) else { return nil }
        return try? JSONDecoder().decode(UserProfile.self, from: profileData)
    }
//    
    func removeProfile() {
        do {
            try keychain.remove(profileKey)
        } catch {
            debugPrint(" ---- error removing profile: \(error)")
        }
    }
    
    func storeUserLoginState(_ isLoggedIn: Bool) {
        UserDefaults.standard.setValue(isLoggedIn, forKey: isLoggedInKey)
    }
    
    func getUserLoginState() -> Bool {
        if let state = UserDefaults.standard.value(forKey: isLoggedInKey) as? Bool {
            return state
        } else {
            return false
        }
    }
    
    func storeDeviceToken(_ token:String) {
        UserDefaults.standard.setValue(token, forKey: deviceTokenKey)
    }
    
    func getDeviceToken()->String{
        UserDefaults.standard.object(forKey: deviceTokenKey) as? String ?? ""
    }
    
    func storePushNotificationValue(_ value:Bool) {
        UserDefaults.standard.setValue(value ? "1" : "0", forKey: pushNotificationValueKey)
    }
    
    func getPushNotificationValue()->Bool{
       let value = UserDefaults.standard.object(forKey: pushNotificationValueKey) as? String ?? "1"
        return value == "1" ? true : false
    }
}
