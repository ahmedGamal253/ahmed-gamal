//
//  MoyaHelpers.swift
//  Connect
//
//  Created by Kerolles Roshdi on 2/10/21.
//

import Foundation
import Moya
import MOLH
class MoyaHelpers {
    
    static func consumeMoyaResult<T: Codable>(_ result: Result<Response, MoyaError>) -> Result<T, Error> {
        switch result {
        case .success(let response):
            let decoder = JSONDecoder()
            switch response.statusCode {
            case 200:
                do {
                    let response = try decoder.decode(T.self, from: response.data)
                    return .success(response)
                } catch {
                    print("response decoding error: \(error)")
                    return .failure(error)
                }
            case 204:
                return .failure(AppError.empty)
            case 400:
                do {
                    let error = try decoder.decode(APIError.self, from: response.data)
                    if error.error == "invalid_grant" { return .failure(AppError.wrongUserNameOrPassword)}
                } catch {
                    print("response decoding error: \(error)")
                }
                return .failure(AppError.badRequest)
            case 401:
                return .failure(AppError.unauthorized)
            case 404:
                return .failure(AppError.notFound)
            case 408:
                return .failure(AppError.timeout)
            case 500:
                return .failure(AppError.networkError)
            default:
                return .failure(AppError.with(message: response.statusCode.description))
            }

        case .failure(let error):
            print("moya error: \(error)")
            if error.errorCode == 6 { return .failure(AppError.offline) }
            return .failure(error)
        }
    }
    
    enum BuildScheme {
        case debug
        case release
    }

    enum NetworkConstant {
        
        enum URLPath: String {
            //case oauth = "oauth"
          //  case Authenticate = "api/Authenticate"
            case api = "api"
        }
        
        case baseURL(_: URLPath)
        case lang
        
        
        static var scheme: BuildScheme = {
            #if DEBUG
            return .debug
            #else
            return .release
            #endif
        }()
        
        
        var value: String {
            
            switch self {
            case .baseURL(let path):
                switch NetworkConstant.scheme {
                case .debug:
                    return "https://connectcoreapi.expertapps.com.sa/\(path)"
                case .release:
                    return "https://connectcoreapi.expertapps.com.sa/\(path)"
                }
                
            case .lang:
                switch MOLHLanguage.currentLocaleIdentifier() {
                case "ar":
                    return "ar"
                default:
                    return "en"
                }
            }
            
        }
    }
    
}
