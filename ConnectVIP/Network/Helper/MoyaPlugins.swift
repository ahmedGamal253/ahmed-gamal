//
//  MoyaPlugins.swift
//  Connect
//
//  Created by Kerolles Roshdi @ ExpertApps on 23/03/2021.
//  Copyright © 2021 Expert Apps. All rights reserved.
//

import Foundation
import Moya

class TokenSource {
  var token: String?
  init() { }
}

struct VariableAccessTokenPlugin: PluginType {
    
    let tokenClosure: () -> String?
       
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        guard let token = tokenClosure(),  let authorizable = target as? AccessTokenAuthorizable, let authorizationType = authorizable.authorizationType else { return request }
        var request = request
        let authValue = authorizationType.value + " " + token
        request.addValue(authValue, forHTTPHeaderField: "Authorization")
        return request
    }
}
