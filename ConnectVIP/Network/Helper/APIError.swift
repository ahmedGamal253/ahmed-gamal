//
//  APIError.swift
//  Connect
//
//  Created by Kerolles Roshdi on 2/10/21.
//

import Foundation

struct APIError: Codable {
    let error : String
}
