//
//  AppError.swift
//  Connect
//
//  Created by Kerolles Roshdi on 2/10/21.
//

import Foundation
import EmptyStateKit

enum AppError: Error, Equatable {
    case offline
    case networkError
    case notFound
    case with(message: String)
    case empty
    case timeout
    case unauthorized
    case badRequest
    case serverError
    case wrongUserNameOrPassword
    case signalRError
    case noMessage
    case noComments
    case forbiddenError(showRetry:Bool = true)
    case noSearchResult
    case noFoundDataWith(title:String ,message:String)
}

extension AppError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .offline:
            return R.string.localizable.error_offline()
        case .networkError:
            return R.string.localizable.error_serverError()
        case .notFound:
            return R.string.localizable.error_notFound()
        case .with(let message):
            return NSLocalizedString(message, comment: "Server error message")
        case .empty:
            return R.string.localizable.error_empty()
        case .timeout:
            return R.string.localizable.error_timeout()
        case .unauthorized:
            return R.string.localizable.error_unauthorized()
        case .badRequest:
            return R.string.localizable.error_badRequest()
        case .serverError:
            return R.string.localizable.error_serverError()
        case .wrongUserNameOrPassword:
            return R.string.localizable.error_wrongUserNameOrPassword()
        case .signalRError:
            return R.string.localizable.error_signalRError()
        case .noMessage:
            return R.string.localizable.no_Message_error_Details()
        case .noComments:
            return R.string.localizable.no_comments_error_Details()
        case .forbiddenError:
            return R.string.localizable.error_forbidden_details()
        case .noSearchResult:
            return R.string.localizable.error_no_search()
        case .noFoundDataWith( _, let message):
            return message
        }
    }
}

extension AppError: CustomState {
    
    var image: UIImage? {
        switch self {
        case .offline:
            return R.image.image_error_offline()
        case .networkError:
            return R.image.image_error_serverError()
        case .notFound:
            return R.image.image_error_networkError()
        case .with:
            return R.image.image_error_serverError()
        case .empty, .noFoundDataWith:
            return R.image.image_error_empty()
        case .timeout:
            return R.image.image_error_timeout()
        case .unauthorized:
            return R.image.image_error_serverError()
        case .badRequest:
            return R.image.image_error_serverError()
        case .serverError:
            return R.image.image_error_serverError()
        case .wrongUserNameOrPassword:
            return nil
        case .signalRError:
            return nil
        case.noMessage ,.noComments:
        return R.image.image_emptyMessages()
        case .forbiddenError:
            return R.image.image_error_offline()
        case.noSearchResult:
            return R.image.error_search()
            
        }
    }
    
    var title: String? {
        switch self {
        case .noMessage:
            return R.string.localizable.no_Message()
        case .noComments:
            return R.string.localizable.no_Comments_or_replies()
        case .forbiddenError:
            return R.string.localizable.error_forbidden_title()
        case .noFoundDataWith(let title,_):
            return title
        default:
            return R.string.localizable.error()
        }
    }
    
    var description: String? {
        return self.errorDescription
    }
    
    var titleButton: String? {
        switch self {
        case .empty, .notFound, .unauthorized,.noMessage , .noComments,.noSearchResult,.noFoundDataWith:
            return nil
        case .forbiddenError(let show):
            return show ? R.string.localizable.closE() : nil
        default: return R.string.localizable.retry()
        }
    }
}
