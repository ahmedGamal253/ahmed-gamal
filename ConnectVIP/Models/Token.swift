//
//  Token.swift
//  ConnectVIP
//
//  Created by ExpertApps Jr. on 22/12/2021.
//

import Foundation

struct Token: Codable {
    let accessToken: String?
    let refreshToken: String?
    enum CodingKeys: String, CodingKey {
        case accessToken = "token"
        case refreshToken

    }
}
