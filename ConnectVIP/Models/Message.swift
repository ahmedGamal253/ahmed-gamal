//
//  Message.swift
//  Connect
//
//  Created by Kerolles Roshdi on 3/25/21.
//  Copyright © 2021 Expert Apps. All rights reserved.
//

import Foundation

struct MessagesResponseModel: Codable {
    let total: Int
    let data: [Message]

    enum CodingKeys: String, CodingKey {
        case total = "totalCount"
        case data = "pageData"
    }
}

struct Message: Codable {
    let lastMessage: String
    let lastMessageDate: String
    let unseenCount: Int
    let group: MessageGroupData?
    let user: MessageUserData?
    
    enum CodingKeys: String, CodingKey {
        case lastMessage = "lastMessageText"
        case lastMessageDate = "lastMessageDate"
        case unseenCount = "unseenCount"
        case group = "messageGroupData"
        case user = "userData"
    }
    
}

struct MessageGroupData: Codable {
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "groupId"
        case name = "groupName"
    }
}

struct MessageUserData: Codable {
    let id: String?
    let name: String
    let imageURL: String?
    let isYou: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "messageCreatorId"
        case name = "messageCreatorName"
        case imageURL = "userProfilePicture"
        case isYou
    }
}


struct DetailedMessage: Codable {
    let id: Int
    let user: MessageUserData?
    let group: MessageGroupData?
    let subject: String?
    let message: String?
    let isSent: Bool?
    let date: String?
    let recipientsCount: Int?
    let commentsNumber: Int?
}

// MARK: - CodingKeys
extension DetailedMessage {
    enum CodingKeys: String, CodingKey {
        case id = "messageId"
        case user = "messageUserData"
        case group = "messageGroupData"
        case subject = "messageTitle"
        case message = "messageContent"
        case isSent = "isMine"
        case date = "messageCreated"
        case recipientsCount = "receiversCount"
        case commentsNumber
    }
}

