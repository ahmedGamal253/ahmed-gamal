//
//  UserProfile.swift
//  Connect
//
//  Created by Kerolles Roshdi @ ExpertApps on 21/03/2021.
//  Copyright © 2021 Expert Apps. All rights reserved.
//

import Foundation

struct UserProfile: Codable {
    let username: String
    let email: String
    let imageURL: String
    let refreshToken: String?
    let id: String
}

// MARK: - CodingKeys
extension UserProfile {
    enum CodingKeys: String, CodingKey {
        case username = "userName"
        case email = "email"
        case imageURL = "profilePicture"
        case refreshToken = "refreshToken"
        case id
    }
}
