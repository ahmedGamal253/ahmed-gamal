//
//  Page.swift
//  Connect
//
//  Created by Kerolles Roshdi on 3/11/21.
//  Copyright © 2021 Expert Apps. All rights reserved.
//

import Foundation

struct Page<T: Codable>: Codable {
    let total: Int
    let data: [T]
}

// MARK: - CodingKeys
extension Page {
    enum CodingKeys: String, CodingKey {
        case total = "totalCount"
        case data = "pageData"
    }
}

// MARK: - AltCodingKeys
extension Page {
    enum AltCodingKeys: String, CodingKey {
        case total = "count"
        case data = "data"
    }
}

extension Page {
    
    init(from decoder: Decoder) throws {

        var total = 0
        var data = [T]()

        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            total = try container.decode(Int.self, forKey: .total)
            data = try container.decode([T].self, forKey: .data)
        } catch is DecodingError {
            let altContainer = try decoder.container(keyedBy: AltCodingKeys.self)
            total = try altContainer.decode(Int.self, forKey: .total)
            data = try altContainer.decode([T].self, forKey: .data)
        }

        self.total = total
        self.data = data
        
    }
    
}
