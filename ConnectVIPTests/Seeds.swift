//
//  File.swift
//  ConnectVIPTests
//
//  Created by ExpertApps Jr. on 03/01/2022.
//

@testable import ConnectVIP
import XCTest

struct Seeds {
  struct Login {
    static let token = Token(accessToken: "testAccessToken", refreshToken: "testRefreshToken")
    }
    
}
